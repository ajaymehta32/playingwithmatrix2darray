package ajaymehta.playingwithmatrix2darray;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/12/2017.
 */

public class PrintingRows {

  static  int[][] matrix;
    static Scanner sc= new Scanner(System.in);

    public static void createMatrix(int row, int column) { // every single row contains all columns element..here row 0th -> 0 ,1,2 column ....row 1th -> 0,1,2 column

        matrix = new int[row][column];

        System.out.println("Enter Matrix Elements ");

        for(int i=0; i<matrix.length; i++) { // row 0th...  it will go upto all rows 3 ..see aboove matrix initilization.. [row] <-- [column]
            for(int j=0; j<matrix[i].length; j++) {  //   column prinitng  ...matrix[i].length .. means .. all columns of ith row .. when i is 0 ..matrix[0].length ...means it will go 0,1,2 ..all columns of 0th row..

                System.out.print("row "+i+" coulmn "+j+": ");
                matrix[i][j] = sc.nextInt();
            }
         } // end of for loop

        printMatrix(matrix);

    }

    public static void printColumn() {

        System.out.println("============ Prininting 1st column ===================");

        for(int i=0; i< matrix.length; i++) {  // we are going through all rows ...

            System.out.println(matrix[i][0]); // i is printing rows ...we took [0] 0th column only for all rows...
        } // end of for loop



        System.out.println("========== Prining 2nd column ==========");

        for(int i=0; i< matrix.length; i++) {

            System.out.println(matrix[i][1]); // i is printing rows ...we took [1] 1th column only for all rows...
        } // end of for loop


        System.out.println("========== Prining Matrix Diganolly left to Right ==========");

        for(int i=0; i< matrix.length; i++) {

            System.out.println(matrix[i][i]); // matrix[row][column] [i] <- row [i] <- column ..here we took i for column also...it will go [0][0] 0th row 0th column , [1][1] -- 0th row 1 coumn ..thats how u print matrix dignoally..
        } // end of for loop


        System.out.println("========== Prining Matrix Diganolly Right to Left ==========");

        for(int i=0; i< matrix.length; i++) {

            System.out.println(matrix[i][matrix[i].length-1-i]); // matrix[i].length -1 is the last column of ith row ..n we add (-i) to it ..when we are at 0th row ..it will print..lastElement-0 -->Last ELement , 1th row ..matrix[1].length -1 (last element) - i (1)  s0...LastElement-i(1) ..will print the second last elemnet n so on..so it created dynamically not static..
        } // end of for loop
    }



    public static void printMatrix(int[][]  matrix) {  // printing all the rows of matrix..

        for (int i = 0; i < matrix.length; i++) {

            for (int j = 0; j < matrix[i].length; j++) {

               System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }

    public static void main(String args[]){


        createMatrix(3,3);
        printColumn();



    }
}
